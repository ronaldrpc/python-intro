def show_message(name, emote, num=1):
    '''

    Returns a message given some parameters (refactor this doc string pls)

    :param name: string
    :param num: int
    :param emote: Emote
    :return: No idea :'v
    '''
    return f'Hi {name}, I\'ll show you {num} emotes {emote}'


def fun(*args, **kwargs):
    '''

    :param args: means positional parameres
    :param kwargs: means ocupational parameters
    :return:
    '''
    print(args)
    print(kwargs)
    # print(args[0], args[1], args[2])
    print(kwargs.get("name"), kwargs.get("rol"), kwargs.get("age", 0))


# fun('hola', 9, True)

params = ['ciao', 10, False]
fun(*params)
print("")
fun(name="Horus", rol="KEKW")
print("")
params_2 = {'name': "Pater", 'nickname': "Who?", 'age': 21}
fun(**params_2)