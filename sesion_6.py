def word_filter_by_length(_list, len_limit):
    '''
    Return a sublist of words that meet the condition of a min len limit given a list of words and the length limit
    '''

    filtered_list = [word for word in _list if len(word) > len_limit]  # < >
    return filtered_list


if __name__ == "__main__":
    word_list = ["hola", 'mundo', ':v', 'madge', 'widepeepo']
    len_limit = int(input("Enter the length limit: "))
    filter_list = word_filter_by_length(word_list, len_limit)
    print(filter_list)
