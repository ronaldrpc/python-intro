result = (lambda a, b: a + b)(4, 5)
# print(result)

another_result = (lambda a, b: a * b)
# print(another_result(1, 3))

result_3 = (lambda *args: args)
_tuple = (1, 2, 3, 4)
# print(result_3(*_tuple))

result_4 = (lambda *args: another_result(*args))


# print(result_4(4, 9))


def cuenta_regresiva(numero):
    numero -= 1
    if numero > 0:
        print(numero)
        cuenta_regresiva(numero)
    else:
        print("Fin de la recursivicad", numero)


# cuenta_regresiva(10)

'''
Ejercicio de fibonacci
'''


def fill_fibonacci_series(previous_num, current_num, remain, first_numbers):
    '''
    Append the sum of two numbers to an array if it meets the condition
    :param previous_num:
    :param current_num:
    :param remain: number of times the function need to call itself (recursion)
    :param first_numbers: array of first [remain+2] fibonacci serie numbers
    :return:
    '''
    if remain > 0:
        next_num = previous_num + current_num
        first_numbers.append(next_num)
        fill_fibonacci_series(current_num, next_num, remain - 1, first_numbers)


def get_first_n_fibonacci(limit):
    '''
    Returns an array that contains the first [limit] fibonacci numbers
    :param limit: the amount of numbers in the array
    :return: An array of numbers
    '''
    first_numbers = [0, 1]
    if limit == 1:
        return [0]
    elif limit == 2:
        return first_numbers

    fill_fibonacci_series(0, 1, limit - 2, first_numbers)
    return first_numbers


if __name__ == '__main__':  # < >
    limit_amount = int(input("Enter the amount of first fibonacci numbers: "))  # Enter the limit amount of numbers
    if limit_amount > 0:
        print(get_first_n_fibonacci(limit_amount))
    else:
        print("Enter a valid limit amount number")


# def validate_limit_amount(limit):
#     return limit > 0

