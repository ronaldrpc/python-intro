'''
Ejercicio 1.
Escribir un programa que pregunte el nombre completo del usuario en la consola y después muestre por pantalla el nombre
completo del usuario tres veces, una con todas las letras minúsculas, otra con todas las letras mayúsculas y otra solo
con la primera letra del nombre y de los apellidos en mayúscula. El usuario puede introducir su nombre combinando
mayúsculas y minúsculas como quiera.
'''

# fullname = input("Enter your fullname: ")
# lowercase_fullname = fullname.lower()
# uppercase_fullname = fullname.upper()
# splitted_fullname = fullname.split(' ')
# thirdcase_fullname = ""
#
# for word in splitted_fullname:
#     new_word = f'{word[0].upper()}{word[1:]} '
#     thirdcase_fullname += new_word
#
# outputs = lowercase_fullname + "\n" + uppercase_fullname + "\n" + thirdcase_fullname
# print(outputs)


'''
Ejercicio 2.
Escribe un programa que pida dos palabras y diga si riman o no. Si coinciden las tres últimas letras tiene que decir que
 riman. Si coinciden sólo las dosúltimas tiene que decir que riman un poco y si no, que no riman.
'''

# first_word = input("Enter the first word: ")
# second_word = input("Enter the second one: ")
# output_rhyme = ""
#
# if first_word[-3:] == second_word[-3:]:
#     output_rhyme = "riman!"
# elif first_word[:-2] == second_word[:-2]:
#     output_rhyme = "riman poco"
# else:
#     output_rhyme = "no riman"
#
# print(f"Las palabras {output_rhyme}")

'''
Ejercicio 3.
Introducir una cadena de caracteres e indicar si es un palíndromo. Una palabra palíndroma es aquella que se lee igual 
adelante que atrás.
'''




